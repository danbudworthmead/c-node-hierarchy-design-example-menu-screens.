#include "Node.h"
Node::Node()
{
	parent_ = nullptr;
}
Node::~Node()
{
	for (auto i = children_.begin(); i != children_.end(); i++)
		RemoveChild(*i);
	parent_ = nullptr;
}
void Node::SetParent(Node* a_parent)
{ 
	if (a_parent == this)
		return;
	if (parent_ != nullptr)
		parent_->RemoveChild(this);
	parent_ = a_parent;
	parent_->AddChild(this);
}
void Node::AddChild(Node* a_child)
{ 
	auto i = children_.find(a_child);
	if (i == children_.end())
		children_.emplace(a_child);
}
void Node::RemoveChild(Node* a_child)
{ 
	auto i = children_.find(a_child);
	if (i != children_.end())
		children_.erase(a_child);
}

std::set<Node*> Node::GetChildren() const
{
	return children_;
}

char* Node::GetName()const
{
	return name_;
}

void Node::SetName(char* a_pName)
{
	name_ = a_pName;
}

Node* Node::GetParent() const
{
	return parent_;
}