#ifndef __NODE_H_
#define __NODE_H_
//\=================================================================================
//\	Includes
//\=================================================================================
#include <set>

class Node
{
	Node* parent_;
	std::set<Node*>children_;
	char* name_;
public:
	Node();
	virtual ~Node();

	void SetParent(Node* a_parent);
	void AddChild(Node* a_child);
	void RemoveChild(Node* a_child);
	std::set<Node*> GetChildren() const;
	Node* GetParent() const;

	char* GetName()const;
	void SetName(char* a_pName);
};

#endif //__NODE_H_