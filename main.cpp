#include "Node.h"
#include <iostream>

auto main(int argv, char* argc[])
{
	//\=================================================================================
	//\	First we are going to create our nodes. These could be a 'menu' class derived 
	//\ from the node class. But for now we'll use the node class.
	//\
	//\ We will also set the names of the nodes (menus).
	//\=================================================================================
	Node application;			application.SetName("Application");
	Node login;					login.SetName("Login");
	Node menu;					menu.SetName("Menu");
	Node joinGame;				joinGame.SetName("Join Game");
	Node server1;				server1.SetName("Server 1");
	Node server2;				server2.SetName("Server 2");
	Node credits;				credits.SetName("Credits");
	Node options;				options.SetName("Options");
	Node options_video;			options_video.SetName("Video Options");
	Node options_game;			options_game.SetName("Game Options");
	Node options_audio;			options_audio.SetName("Audio Options");
	Node quit;					quit.SetName("Quit");

	//\=================================================================================
	//\	Next we will set the parents of each node. This adds the node to the parents
	//\ vector of children. 
	//\ Each parent has their own list of children and each child has one parent.
	//\ (except application which is the top node in the hierarchy)
	//\=================================================================================
	quit.SetParent(&menu);
	options_audio.SetParent(&options);
	options_game.SetParent(&options);
	options_video.SetParent(&options);
	options.SetParent(&menu);
	credits.SetParent(&menu);
	server1.SetParent(&joinGame);
	server2.SetParent(&joinGame);
	joinGame.SetParent(&menu);
	menu.SetParent(&login);
	login.SetParent(&application);

	//\=================================================================================
	//\ We are going to have a pointer to the current node to keep track of what node
	//\ we are currently using. We will set the current node to the top most node in
	//\ the hierarchy.
	//\=================================================================================
	Node* pCurrentNode = &application;

	//\=================================================================================
	//\ We will be using a do while loop for the game loop.
	//\=================================================================================
	do 
	{
		//\=================================================================================
		//\ First we are going to print out the name of the current node (menu screen)
		//\=================================================================================
		std::cout << "=== " << pCurrentNode->GetName() << " ===" << std::endl;

		//\=================================================================================
		//\ Next we are going to print out all of the current node's children.
		//\=================================================================================
		for (Node* iter : pCurrentNode->GetChildren())
		{
			std::cout << "- " << iter->GetName() << std::endl;
		}

		//\=================================================================================
		//\ Here we are going to get the user's input.
		//\=================================================================================
		std::cout << "\n\nNode Choice ('..' to go up a node): ";
		char input[256];
		std::cin.getline(input, 256);

		//\=================================================================================
		//\ If the input is ".." then we will go up a node by setting the current node to 
		//\ it's own parent.
		//\=================================================================================
		if (strcmp(input, "..") == 0)
		{
			//\=================================================================================
			//\ We need to make sure that the current node has a parent before we access it.
			//\=================================================================================
			if (pCurrentNode->GetParent())
			{
				//\=================================================================================
				//\ Setting the current node to the current node's parent. Thereby going up 
				//\ the hierarchy by one node.
				//\=================================================================================
				pCurrentNode = pCurrentNode->GetParent();
			}
		}
		else
		{
			//\=================================================================================
			//\ If the input is not ".." then we will compare the input to each of the current
			//\ nodes children and if the input matches the child's name we will set the 
			//\ current node to that child.
			//\=================================================================================
			for (Node* iter : pCurrentNode->GetChildren())
			{
				//\=================================================================================
				//\ We are using _stricmp for the time being to compare the strings. This is only
				//\ supported on Windows. If you want portability, use either strcmp or write your
				//\ own string comparison function.
				//\=================================================================================
				if (_stricmp(input, iter->GetName()) == 0)
				{
					pCurrentNode = iter;
				}
			}
		}
		//\=================================================================================
		//\	Lastly we are going to clear the screen ready for the next iteration of the 
		//\ game loop.
		//\=================================================================================
		system("cls");
	} while (true);
	return 0;
}
